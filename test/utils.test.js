import generateToken from '../src/utils/generateToken'
import jwt from 'jsonwebtoken'

const user = {
    id: 5
}

test('Testing generate token function', () => {
    const token = generateToken(user.id)
    const decoded = jwt.decode(token, process.env.JWT_SECRET)

    expect(decoded.userId).toBe(user.id)
})