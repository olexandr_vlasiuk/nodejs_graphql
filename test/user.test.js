import prisma from '../src/prisma'
import getUser, {getUserByName} from '../src/utils/getUser'

let uId = '';
const user = {
    name: 'test',
    nickname: 'test1' 
}

test('Check query method', async () => {
    const data = {
        email: 'admin@test.com'
    }
    const admin = await prisma.query.admin({
        where: {
            email: data.email
        }
    })

    expect(admin.name).toBe('admin')
})

test('Add user to DB', async() => {  
    const savedUser = await prisma.mutation.createUser({data: user})
    uId = savedUser.id;

    expect(savedUser.name).toBe(user.name)
})

test('Get user by id', async() => {
    const searchedUser = await getUser(uId)

    expect(searchedUser.name).toBe(user.name)
})

test('Get user by name', async() => {
    const searchedUser = await getUserByName(user.nickname)
    
    expect(searchedUser.id).toBe(uId)
})

test('Delete user' , async() => {
    await prisma.mutation.deleteUser({
         where: {
            id: uId
        }
    })
    const IsUser = await prisma.exists.User({
        id: uId
    })

    expect(IsUser).toBe(false)
})
