import getReddit from '../utils/getReddit'
import getUserId from "../utils/getUserId";

const Query = {
    users(parent, args, {request, prisma}, info) {
        getUserId(request)

        const opArgs = {
            first: args.first,
            skip: args.skip,
            after: args.after
        }

        if(args.query) 
            opArgs.where = {
                name_contains: args.query
            }

        return prisma.query.users(opArgs, info)
    },
    reddits(parent, args, {request, prisma}, info) {
        getUserId(request)

        const opArgs = {
            first: args.first,
            skip: args.skip,
            after: args.after
        }

        if(args.query) 
            opArgs.where = {
                title_contains: args.query
            }

        return prisma.query.reddits(opArgs, info)
    },
    async posts(parent, args, {request, prisma}, info) {
        getUserId(request)

        const opArgs = {
            first: args.first,
            skip: args.skip,
            after: args.after
        }

        if(args.query) {
            const reddit = await getReddit(args.query)
            if(!reddit)
                throw new Error('No reddit with such title')
            opArgs.where = {
                category: {
                    id: reddit.id
                }                 
            }
            opArgs.orderBy = "createdAt_DESC" 
        }

        return prisma.query.posts(opArgs, info)
    },
    post(parent, args, {prisma}, info) {
        getUserId(request)

        const opArgs = {
            where: {
                id: args.id
            }
        }

        return prisma.query.post(opArgs, info)
    },
    search(parent, args, {request, prisma}, info){
        getUserId(request)
        
        const opArgs = {
            first: args.first,
            skip: args.skip,
            after: args.after
        }

        if(args.query) {
            opArgs.where = {
                OR : [{
                    title_contains: args.query
                    }, {
                        body_contains: args.query
                }]
            }
        }

        return prisma.query.posts(opArgs, info)
    }
}

export {Query as default}