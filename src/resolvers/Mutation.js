import bcrypt from 'bcryptjs'
import generatToken from '../utils/generateToken';
import hashPassword from '../utils/hashPassword';
import getUser from "../utils/getUser";
import getReddit from "../utils/getReddit";
import getPost from "../utils/getPost";
import getUserId from "../utils/getUserId";

const Mutation = {
    async signUp(parent, args, { prisma }, info) {
        const password = await hashPassword(args.data.password)

        const user = await prisma.mutation.createAdmin({ 
            data: {
                ...args.data,
                password
            } 
        })

        return {
            user,
            token: generatToken(user.id)
        }
    },
    async login(parent, args, {prisma}, info) {
        const admin = await prisma.query.admin({
            where: {
                email: args.data.email
            }
        })

        if(!admin) 
            throw new Error('No user with this email')

        const isMatch = await bcrypt.compare(args.data.password, admin.password)

        if(!isMatch) 
            throw new Error('Wrong password')

        return {
            admin,
            token: generatToken(admin.id)
        }
    },
    createUser(parent, args, { prisma }, info) {      
        return prisma.mutation.createUser({data: {...args.data}}, info)
    },
    async deleteUser(parent, args, { prisma }, info) {
        getUserId(request)

        const userExists = await prisma.exists.User({id: args.id})

        if(!userExists)
            throw new Error('No user with this ID')

        return prisma.mutation.deleteUser({
            where: 
                {
                    id: args.id
                }
        }, info)
    },
    createReddit(parent, args, {prisma}, info) {
        return prisma.mutation.createReddit({data: {...args.data}}, info)
    },
    async deleteReddit(parent, args, { prisma }, info) {
        getUserId(request)

        const redditExists = await prisma.exists.Reddit({id: args.id})

        if(!redditExists)
            throw new Error('No user with this ID')

        return prisma.mutation.deleteReddit({
            where: 
                {
                    id: args.id
                }
        }, info)
    },
    async createPost(parent, args, {prisma}, info) {
        const user = await getUser(args.data.author)
        if(!user)
            throw new Error('Wrong author nickname')
        
        const reddit = await getReddit(args.data.category)
        if(!reddit) 
            throw new Error('Wrong category name')

        return prisma.mutation.createPost({data: {
            title: args.data.title,
            body: args.data.body,
            author: {
                connect: {
                    id: user.id
                }
            },
            category: {
                connect: {
                    id: reddit.id
                }
            }
        }}, info)
        
    },
    async updatePost(parent, args, { prisma }, info) {
        getUserId(request)

        const postExists = await prisma.exists.Post({
            id: args.id
        })

        if(!postExists) {
            throw new Error('Unable to update post')
        }

        return prisma.mutation.updatePost({
            where: {
                id: args.id
            },
            data:args. data
        },info)
    },
    async deletePost(parent, args, {request, prisma}, info) {
        getUserId(request)
        const post = await getPost(args.id)
        if(!post)
            throw new Error('Post not exists')

        return prisma.mutation.deletePost({
            where: {
                id: args.id
            }
      })
    } 
}

export {Mutation as default}