import { GraphQLServer } from 'graphql-yoga'
import { resolvers, fragmentReplacements }from './resolvers/index'
import prisma from './prisma'
import cron from './cron/getReditData'

// Start cron for getting info from reddit
cron()

const server = new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers,
    context(request) {
        return {
            prisma,
            request
        }
    },
    fragmentReplacements
})

export {server as default}