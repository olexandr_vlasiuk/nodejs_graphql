import prisma from '../prisma'

const getPost = id =>
    prisma.query.post({
        where: {
            id
        }
    })

export {getPost as default}