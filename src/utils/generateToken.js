import jwt from 'jsonwebtoken'

const secret = process.env.JWT_SECRET
const ttl = { expiresIn: '7 days'}

const generatedToken = userId => jwt.sign({ userId }, secret, ttl)

export { generatedToken as default }