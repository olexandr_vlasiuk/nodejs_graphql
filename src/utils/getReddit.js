import prisma from '../prisma'

const getReddit = id => 
    prisma.query.reddit({
        where: {
            id
        }
    })

const getRedditByTitle = title => 
    prisma.query.reddit({
        where: {
            title
        }
    })

export {getReddit as default, getRedditByTitle}