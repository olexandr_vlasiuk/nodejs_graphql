import prisma from '../prisma'

const getUser = id => 
    prisma.query.user({
        where: {
            id
        }            
    })

const getUserByName = name =>
    prisma.query.user({
        where: {
            nickname: name
        }            
    })

export {getUser as default, getUserByName}