import Snoowrap from 'snoowrap'
import Cron from 'node-cron'
import prisma from '../prisma'
import { getUserByName } from '../utils/getUser'
import { getRedditByTitle } from '../utils/getReddit'

const r = new Snoowrap({
    userAgent: 'Get historical from front page',
    clientId: process.env.R_CLIENT_ID,
    clientSecret: process.env.R_CLIENT_SECRET,
    username: process.env.R_USERNAME,
    password: process.env.R_PASSWORD
});

const createUser = data => prisma.mutation.createUser({data}) 

const createReddit = data => prisma.mutation.createReddit({data})

const saveRedditData = posts => {
    posts.forEach(async (value) => {
        let user = await getUserByName(value.author)        
        if(!user) 
            user = await createUser({
                name: value.author_fullname,
                nickname: value.author
            }) 

        let reddit = await getRedditByTitle(value.subreddit)
        if(!reddit) 
            reddit = await createReddit({
                title: value.subreddit
            })

        return prisma.mutation.createPost({
            data: {
                title: value.title,
                body: value.selftext,
                author: {
                    connect: {
                        id: user.id
                    }
                },
                category: {
                    connect: {
                        id: reddit.id
                    }
                }
            }
        })
    });    
}

const getNewRedditData = () => {
    r.getNew().map(post => {
        return { 
            author: post.author.name,
            author_fullname: post.author_fullname,
            subreddit: post.subreddit.display_name,
            title: post.title,
            selftext: post.selftext
        }
    }).then(saveRedditData)
}

const cron = () => {
    console.log('CRON started')
    Cron.schedule('00 * */1 * * *', getNewRedditData)
}

export {cron as default}