# NodeJS_GraphQL

Requirements:    
- docker  
- prisma  
- nodejs  

##Docker IP

In widndows {Docker generated IP} = 192.168.99.100  
MacOS, Linux {Docker generated IP} = localhost  

##Project configuration  

1. Need to create config file in config folder `./config/dev.env`      
2. Set params `  
    PRISMA_ENDPOINT={Docker generated IP}:4466/nodejstest/dev  
    R_CLIENT_ID: ******  
    R_CLIENT_SECRET: ******  
    R_USERNAME: ******  
    R_PASSWORD: ******  
    `  

##Check workflow:  

1. Generate token `prisma token -e ../config/dev.env` from prisma folder  
2. Open DB playground: {Docker generated IP}:4466/nodejstest/dev  
3. Set Header `{ "Authorization": "Bearer {token}"}`  
4. Create simple request `query{   
    users {  
        name   
        nickname   
    }  
}`  
 
##Check functions unsing JEST  

Run command `npm run test`  

##Start server  

Run command `npm run start`  



to deploy db changes  

From ./prisma directory  
`prisma deploy -e ../config/dev.env`    


All endpoints on page: `localhost:4000`  